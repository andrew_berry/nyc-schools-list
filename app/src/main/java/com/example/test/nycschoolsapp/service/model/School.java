package com.example.test.nycschoolsapp.service.model;

import com.google.gson.annotations.SerializedName;

public class School {

    @SerializedName("school_name")
    public String schoolName;

    @SerializedName("city")
    public String city;

    @SerializedName("zip")
    public String zip;

    @SerializedName("dbn")
    public String dbn;

    public School() {
    }

    public School(String schoolName) {
        this.schoolName = schoolName;
    }
}
