package com.example.test.nycschoolsapp.service.repository;

import com.example.test.nycschoolsapp.service.model.School;
import com.example.test.nycschoolsapp.service.model.SchoolSATScores;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NYCSchoolService {
    String HTTPS_API_GITHUB_URL = "https://data.cityofnewyork.us/resource/";

    @GET("s3k6-pzi2.json")
    Call<List<School>> getSchoolList();

    @GET("f9bf-2cp4.json")
    Call<List<SchoolSATScores>> getSchoolDetails(@Query("dbn") String dbn);
}
