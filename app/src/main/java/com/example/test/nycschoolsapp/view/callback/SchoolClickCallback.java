package com.example.test.nycschoolsapp.view.callback;

import com.example.test.nycschoolsapp.service.model.School;

public interface SchoolClickCallback {
    void onClick(School school);
}
