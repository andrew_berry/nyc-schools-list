package com.example.test.nycschoolsapp.service.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.test.nycschoolsapp.service.model.School;
import com.example.test.nycschoolsapp.service.model.SchoolSATScores;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@Singleton
public class SchoolRepository {
    private NYCSchoolService NYCSchoolService;

    @Inject
    public SchoolRepository(NYCSchoolService NYCSchoolService) {
        this.NYCSchoolService = NYCSchoolService;
    }

    public LiveData<List<School>> getSchoolList() {
        final MutableLiveData<List<School>> data = new MutableLiveData<>();

        NYCSchoolService.getSchoolList().enqueue(new Callback<List<School>>() {
            @Override
            public void onResponse(Call<List<School>> call, Response<List<School>> response) {
                data.setValue(response.body());
            }

            @Override
            public void onFailure(Call<List<School>> call, Throwable t) {
                // TODO better error handling in part #2 ...
                data.setValue(null);
            }
        });

        return data;
    }

    public LiveData<List<SchoolSATScores>> getSchoolSATScores(String dbn) {
        final MutableLiveData<List<SchoolSATScores>> data = new MutableLiveData<>();

        NYCSchoolService.getSchoolDetails(dbn).enqueue(new Callback<List<SchoolSATScores>>() {
            @Override
            public void onResponse(Call<List<SchoolSATScores>> call, Response<List<SchoolSATScores>> response) {
                simulateDelay();
                data.setValue(response.body());
            }

            @Override
            public void onFailure(Call<List<SchoolSATScores>> call, Throwable t) {
                // TODO better error handling in part #2 ...
                data.setValue(null);
            }
        });

        return data;
    }

    private void simulateDelay() {
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
