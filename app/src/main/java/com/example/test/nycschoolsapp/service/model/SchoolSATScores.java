package com.example.test.nycschoolsapp.service.model;

import com.google.gson.annotations.SerializedName;

public class SchoolSATScores {

    @SerializedName("school_name")
    public String schoolName;

    @SerializedName("num_of_sat_test_takers")
    public String numSATTestTakers;

    @SerializedName("sat_critical_reading_avg_score")
    public String satReadingScore;

    @SerializedName("sat_math_avg_score")
    public String satMathScore;

    @SerializedName("sat_writing_avg_score")
    public String satWritingScore;
}