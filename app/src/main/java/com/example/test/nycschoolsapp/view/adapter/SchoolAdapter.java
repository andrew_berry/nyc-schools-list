package com.example.test.nycschoolsapp.view.adapter;

import androidx.databinding.DataBindingUtil;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.test.nycschoolsapp.R;
import com.example.test.nycschoolsapp.databinding.SchoolListItemBinding;
import com.example.test.nycschoolsapp.service.model.School;
import com.example.test.nycschoolsapp.view.callback.SchoolClickCallback;

import java.util.List;

//Adapter used to display list with various New York Schools as list items
public class SchoolAdapter extends RecyclerView.Adapter<SchoolAdapter.SchoolViewHolder> {

    List<? extends School> schoolList;

    @Nullable
    private final SchoolClickCallback schoolClickCallback;

    public SchoolAdapter(@Nullable SchoolClickCallback schoolClickCallback) {
        this.schoolClickCallback = schoolClickCallback;
    }

    public void setSchoolList(final List<? extends School> schoolList) {
        if (this.schoolList == null) {
            this.schoolList = schoolList;
            notifyItemRangeInserted(0, schoolList.size());
        } else {
            DiffUtil.DiffResult result = DiffUtil.calculateDiff(new DiffUtil.Callback() {
                @Override
                public int getOldListSize() {
                    return SchoolAdapter.this.schoolList.size();
                }

                @Override
                public int getNewListSize() {
                    return schoolList.size();
                }

                @Override
                public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                    return SchoolAdapter.this.schoolList.get(oldItemPosition).schoolName ==
                            schoolList.get(newItemPosition).schoolName;
                }

                @Override
                public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                    School school = schoolList.get(newItemPosition);
                    School old = schoolList.get(oldItemPosition);
                    return school.schoolName == old.schoolName;
                }
            });
            this.schoolList = schoolList;
            result.dispatchUpdatesTo(this);
        }
    }

    @Override
    public SchoolViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        SchoolListItemBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.school_list_item,
                        parent, false);

        binding.setCallback(schoolClickCallback);

        return new SchoolViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(SchoolViewHolder holder, int position) {
        holder.binding.setSchool(schoolList.get(position));
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return schoolList == null ? 0 : schoolList.size();
    }

    static class SchoolViewHolder extends RecyclerView.ViewHolder {

        final SchoolListItemBinding binding;

        public SchoolViewHolder(SchoolListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
