package com.example.test.nycschoolsapp.di;

import com.example.test.nycschoolsapp.service.repository.NYCSchoolService;
import com.example.test.nycschoolsapp.viewmodel.SchoolViewModelFactory;

import javax.inject.Singleton;

import androidx.lifecycle.ViewModelProvider;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module(subcomponents = ViewModelSubComponent.class)
class AppModule {
    @Singleton @Provides
    NYCSchoolService provideGithubService() {
        return new Retrofit.Builder()
                .baseUrl(NYCSchoolService.HTTPS_API_GITHUB_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(NYCSchoolService.class);
    }

    @Singleton
    @Provides
    ViewModelProvider.Factory provideViewModelFactory(
            ViewModelSubComponent.Builder viewModelSubComponent) {

        return new SchoolViewModelFactory(viewModelSubComponent.build());
    }
}
