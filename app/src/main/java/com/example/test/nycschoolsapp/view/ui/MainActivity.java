package com.example.test.nycschoolsapp.view.ui;

import android.os.Bundle;

import com.example.test.nycschoolsapp.R;
import com.example.test.nycschoolsapp.service.model.School;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

public class MainActivity extends AppCompatActivity implements HasSupportFragmentInjector {

    @Inject
    DispatchingAndroidInjector<Fragment> dispatchingAndroidInjector;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Add project list fragment if this is first creation
        if (savedInstanceState == null) {
            SchoolListFragment fragment = new SchoolListFragment();

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, fragment, SchoolListFragment.TAG).commit();
        }
    }

    /** Shows the school SAT scores fragment */
    public void show(School school) {
        SchoolSATScoresFragment schoolSATScoresFragment = SchoolSATScoresFragment.forProject(school.dbn);

        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack("school")
                .replace(R.id.fragment_container,
                        schoolSATScoresFragment, null).commit();
    }

    @Override
    public DispatchingAndroidInjector<Fragment> supportFragmentInjector() {
        return dispatchingAndroidInjector;
    }
}
