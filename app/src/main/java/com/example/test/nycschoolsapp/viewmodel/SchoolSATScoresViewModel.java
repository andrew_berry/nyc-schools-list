package com.example.test.nycschoolsapp.viewmodel;

import android.app.Application;
import android.util.Log;

import com.example.test.nycschoolsapp.service.model.SchoolSATScores;
import com.example.test.nycschoolsapp.service.repository.SchoolRepository;

import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

//View model that corresponds to SchoolSATScores fragment
public class SchoolSATScoresViewModel extends AndroidViewModel {
    private static final String TAG = SchoolSATScoresViewModel.class.getName();
    private static final MutableLiveData ABSENT = new MutableLiveData();
    {
        //noinspection unchecked
        ABSENT.setValue(null);
    }

    private final LiveData<List<SchoolSATScores>> schoolSATScoresObservable;
    private final MutableLiveData<String> dbnName;

    public ObservableField<SchoolSATScores> schoolSATScores = new ObservableField<SchoolSATScores>();

    @Inject
    public SchoolSATScoresViewModel(@NonNull SchoolRepository schoolRepository, @NonNull Application application) {
        super(application);

        this.dbnName = new MutableLiveData<>();

        schoolSATScoresObservable = Transformations.switchMap(dbnName, input -> {
            if (input.isEmpty()) {
                Log.i(TAG, "ProjectViewModel projectID is absent!!!");
                return ABSENT;
            }

            Log.i(TAG,"ProjectViewModel projectID is " + dbnName.getValue());

            return schoolRepository.getSchoolSATScores(dbnName.getValue());
        });
    }

    public LiveData<List<SchoolSATScores>> getObservableProject() {
        return schoolSATScoresObservable;
    }

    public void setSchoolSATScores(SchoolSATScores schoolSATScores) {
        this.schoolSATScores.set(schoolSATScores);
    }

    public void setDbnName(String dbnName) {
        this.dbnName.setValue(dbnName);
    }
}
