package com.example.test.nycschoolsapp.di;

import com.example.test.nycschoolsapp.view.ui.SchoolSATScoresFragment;
import com.example.test.nycschoolsapp.view.ui.SchoolListFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentBuildersModule {
    @ContributesAndroidInjector
    abstract SchoolSATScoresFragment contributeProjectFragment();

    @ContributesAndroidInjector
    abstract SchoolListFragment contributeProjectListFragment();
}
