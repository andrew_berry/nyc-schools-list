package com.example.test.nycschoolsapp.view.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.test.nycschoolsapp.R;
import com.example.test.nycschoolsapp.databinding.FragmentSchoolSatScoresBinding;
import com.example.test.nycschoolsapp.di.Injectable;
import com.example.test.nycschoolsapp.service.model.SchoolSATScores;
import com.example.test.nycschoolsapp.viewmodel.SchoolSATScoresViewModel;

import java.util.List;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

//fragment for showing details of a selected school including SAT Scores
public class SchoolSATScoresFragment extends Fragment implements Injectable {
    private static final String KEY_DBN = "dbn";
    private FragmentSchoolSatScoresBinding binding;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        // Inflates the data binding layout
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_school_sat_scores, container, false);

        // Creates RecyclerView and sets adapter.
        return (View) binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final SchoolSATScoresViewModel viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(SchoolSATScoresViewModel.class);

        viewModel.setDbnName(getArguments().getString(KEY_DBN));

        binding.setSchoolSATScoresViewModel(viewModel);
        binding.setIsLoading(true);

        observeViewModel(viewModel);
    }

    private void observeViewModel(final SchoolSATScoresViewModel viewModel) {
        // Observing project data
        viewModel.getObservableProject().observe(this, new Observer<List<SchoolSATScores>>() {
            @Override
            public void onChanged(@Nullable List<SchoolSATScores> schoolSATScores) {
                if (schoolSATScores != null) {
                    binding.setIsLoading(false);
                    if (!schoolSATScores.isEmpty()) {
                        viewModel.setSchoolSATScores(schoolSATScores.get(0));
                    } else {
                        viewModel.setSchoolSATScores(null);
                    }
                }
            }
        });
    }

    /** Creates project fragment for specific dbn number*/
    public static SchoolSATScoresFragment forProject(String dbn) {
        SchoolSATScoresFragment fragment = new SchoolSATScoresFragment();
        Bundle args = new Bundle();

        args.putString(KEY_DBN, dbn);
        fragment.setArguments(args);

        return fragment;
    }
}
