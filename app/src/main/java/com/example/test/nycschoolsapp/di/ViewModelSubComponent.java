package com.example.test.nycschoolsapp.di;

import com.example.test.nycschoolsapp.viewmodel.SchoolListViewModel;
import com.example.test.nycschoolsapp.viewmodel.SchoolSATScoresViewModel;
import com.example.test.nycschoolsapp.viewmodel.SchoolViewModelFactory;

import dagger.Subcomponent;

/**
 * A sub component to create ViewModels. It is called by the
 * {@link SchoolViewModelFactory}.
 */
@Subcomponent
public interface ViewModelSubComponent {
    @Subcomponent.Builder
    interface Builder {
        ViewModelSubComponent build();
    }

    SchoolListViewModel schoolListViewModel();
    SchoolSATScoresViewModel schoolSATScoresViewModel();
}
