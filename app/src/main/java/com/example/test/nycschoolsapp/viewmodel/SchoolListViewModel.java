package com.example.test.nycschoolsapp.viewmodel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.annotation.NonNull;

import com.example.test.nycschoolsapp.service.model.School;
import com.example.test.nycschoolsapp.service.repository.SchoolRepository;

import java.util.List;

import javax.inject.Inject;

//ViewModel that corresponds to school list fragment
public class SchoolListViewModel extends AndroidViewModel {
    private final LiveData<List<School>> schoolListObservable;

    @Inject
    public SchoolListViewModel(@NonNull SchoolRepository schoolRepository, @NonNull Application application) {
        super(application);

        schoolListObservable = schoolRepository.getSchoolList();
    }

    /**
     * Exposes the LiveData Projects query so it can be observed by the UI.
     */
    public LiveData<List<School>> getSchoolListObservable() {
        return schoolListObservable;
    }
}
