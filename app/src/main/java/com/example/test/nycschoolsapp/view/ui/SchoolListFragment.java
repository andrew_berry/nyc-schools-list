package com.example.test.nycschoolsapp.view.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.test.nycschoolsapp.R;
import com.example.test.nycschoolsapp.databinding.FragmentSchoolListBinding;
import com.example.test.nycschoolsapp.di.Injectable;
import com.example.test.nycschoolsapp.service.model.School;
import com.example.test.nycschoolsapp.view.adapter.SchoolAdapter;
import com.example.test.nycschoolsapp.view.callback.SchoolClickCallback;
import com.example.test.nycschoolsapp.viewmodel.SchoolListViewModel;

import java.util.List;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

//Fragment for displaying the list of schools
public class SchoolListFragment extends Fragment implements Injectable {
    public static final String TAG = "SchoolListFragment";
    private SchoolAdapter schoolAdapter;
    private FragmentSchoolListBinding binding;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_school_list, container, false);

        schoolAdapter = new SchoolAdapter(schoolClickCallback);
        binding.schoolList.setAdapter(schoolAdapter);
        binding.setIsLoading(true);

        return (View) binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final SchoolListViewModel viewModel = ViewModelProviders.of(this,
                viewModelFactory).get(SchoolListViewModel.class);

        observeViewModel(viewModel);
    }

    private void observeViewModel(SchoolListViewModel viewModel) {
        // Updates list when the data changes
        viewModel.getSchoolListObservable().observe(this, new Observer<List<School>>() {
            @Override
            public void onChanged(@Nullable List<School> schools) {
                if (schools != null) {
                    binding.setIsLoading(false);
                    schoolAdapter.setSchoolList(schools);
                }
            }
        });
    }

    private final SchoolClickCallback schoolClickCallback = new SchoolClickCallback() {
        @Override
        public void onClick(School school) {
            if (getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.STARTED)) {
                ((MainActivity) getActivity()).show(school);
            }
        }
    };
}
